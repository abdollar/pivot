require 'rails_helper'

RSpec.describe "tasks/edit", type: :view do
  before(:each) do
    @user = assign(:user, User.create!(:nickname => 'user1'))
    @task = assign(:task, Task.create!(:description => 'description1', :user_id => @user.id))
  end

  it "renders the edit task form" do
    render

    assert_select "form[action=?][method=?]", task_path(@task), "post" do
    end
  end
end
