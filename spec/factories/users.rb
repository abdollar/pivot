FactoryGirl.define do
  factory :user do
    sequence(:nickname) { |n| "user#{n}" }
    password  "test"
  end
end
