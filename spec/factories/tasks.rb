FactoryGirl.define do
  factory :task do
    sequence(:description) { |n| "description #{n}" }
    association(:user)
  end
end
