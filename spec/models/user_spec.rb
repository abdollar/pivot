require 'rails_helper'

RSpec.describe User, type: :model do

  context "validation" do

    before do
      @user = User.new(:nickname => 'user1')
    end

    it "requires nickname" do
      expect(@user).to validate_presence_of(:nickname)
    end

  end

end
