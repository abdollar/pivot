require 'rails_helper'

RSpec.describe Task, type: :model do
  context "validation" do

    before do
      @user = User.new(:nickname => 'name1')
      @task = Task.new(:description => 'description 1', :user_id => @user.id)
    end

    it "requires description" do
      expect(@task).to validate_presence_of(:description)
    end

  end
end
