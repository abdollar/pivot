class User < ActiveRecord::Base
  validates :nickname, presence: true
  has_many :tasks
end
