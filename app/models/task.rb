class Task < ActiveRecord::Base
  scope :complete, -> { where("completed_at is not null") }
  scope :incomplete, -> { where(completed_at: nil) }
  validates :description, presence: true
  belongs_to :user
  validates_presence_of :user_id
end
