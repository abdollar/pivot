class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.string :completed_at
      t.string :description
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
